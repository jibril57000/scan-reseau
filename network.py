import socket 
def scanNetwork():
    ip,host=socket.gethostbyname(socket.gethostname())[0:10],[]
    for i in range(255):
        try:host.append(socket.gethostbyaddr(ip+str(i)))
        except socket.herror:pass
    maximum, maximumHost = (max(len(host[0] + host[2][0]) for host in host),max(len(host[0]) for host in host),)
    print("┏" + "━"*(maximumHost+2) +"╋"+"━"*maximum +"┓")
    for host in host:
        print("┃🟢"+host[0]+" "*(maximumHost-len(host[0]))+"┃" +host[2][0] +" "*(maximum-len(host[2][0]))+"┃")
        print("┣" + "━"*(maximumHost+2) +"╋"+"━"*maximum +"┫")

scanNetwork()